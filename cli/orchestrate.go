package main

import (
	"github.com/sayden/form3-cc"
	"github.com/sayden/form3-cc/http"
	"github.com/sayden/form3-cc/logging"
	"github.com/sayden/form3-cc/storage/memory"
	"github.com/thehivecorporation/log"
	"time"
)

func orchestrate(c *form3_cc.Config) {
	//Storage
	pm := memory.New()

	//Decorate storage with Logging and telemetry (this pattern can be used for many purposes like data integrity)
	logger, err := logging.New(pm)
	if err != nil {
		log.WithError(err).Fatal("Error creating logger layer")
	}

	//Persist payments every 5 seconds
	go func() {
		for {
			time.Sleep(5 * time.Second)
			if err := logger.Persist(); err != nil {
				log.WithError(err).Error("Error trying to persist data")
			}
		}
	}()

	//Initialize server
	rest, err := http.NewPaymentServer(c.Port, logger)
	if err != nil {
		log.WithError(err).Fatal("Error creating server")
	}

	// Launch server (blocking)
	rest.Run()
}
