package main

import (
	"github.com/sayden/form3-cc"
	"github.com/timjchin/unpuzzled"
	"os"
)

func launchCLI() {
	config := new(form3_cc.Config)

	app := unpuzzled.NewApp()
	app.Command = &unpuzzled.Command{
		Variables: []unpuzzled.Variable{
			&unpuzzled.IntVariable{
				Name:        "port",
				Destination: &config.Port,
			},
		},
		Action: func() {
			orchestrate(config)
		},
	}

	app.Run(os.Args)
}
