package main

import (
	"testing"
	"github.com/sayden/form3-cc"
	"net/http"
	"github.com/gin-gonic/gin/json"
	"bytes"
	"time"
	"github.com/thehivecorporation/log"
	"github.com/sayden/form3-cc/storage/memory"
	paymentsServer "github.com/sayden/form3-cc/http"
)

func TestRESTPaymentGinServer_Run(t *testing.T) {
	port := 8081
	endpoint := "http://localhost:8081/api/v1/payment"

	payment1 := form3_cc.Payment{
		ID: "id1",
		Attributes: form3_cc.PaymentAttributes{
			Amount: "100",
		},
	}

	server, err := paymentsServer.NewPaymentServer(port, memory.New())
	if err != nil {
		t.Fatal(err)
	}

	t.Run("No nil payment manager", func(t *testing.T) {
		_, err := paymentsServer.NewPaymentServer(port, nil)
		if err == nil {
			t.Error(err)
			t.Fail()
		}
	})

	go server.Run()
	defer server.Stop()

	t.Run("Number of payments in the list must be zero", func(t *testing.T) {
		ps, err := server.List()
		if err != nil {
			t.Fatal(err)
		}

		if len(ps.Data) != 0 {
			t.Fatalf("No payments should be found. Found %d payments", len(ps.Data))
		}
	})

	t.Run("CREATE Payment", func(t *testing.T) {
		t.Run("Check payment creation. Must return a success response", func(t *testing.T) {

			byt, err := json.Marshal(payment1)
			if err != nil {
				t.Fatal(err)
			}

			reader := bytes.NewReader(byt)
			req, err := http.NewRequest(http.MethodPut, endpoint, reader)
			if err != nil {
				t.Fatalf("Error trying to create request: %s", err.Error())
			}

			client := http.DefaultClient
			client.Timeout = time.Second * 5

			res, err := client.Do(req)
			if err != nil {
				log.Fatal(err)
			}

			response := form3_cc.HTTPResponse{}
			if err = json.NewDecoder(res.Body).Decode(&response); err != nil {
				t.Fatal(err)
			}
			res.Body.Close()

			if !response.Success {
				t.Fatal("Expected successful response")
			}

			t.Run("Deduplication is performed after a Persist call, so we should be able to insert the same payment again without error", func(t *testing.T) {
				reader := bytes.NewReader(byt)
				req, err := http.NewRequest(http.MethodPut, endpoint, reader)
				if err != nil {
					t.Fatalf("Error trying to create request: %s", err.Error())
				}

				res, err := client.Do(req)
				if err != nil {
					log.Fatal(err)
				}

				response := form3_cc.HTTPResponse{}
				if err = json.NewDecoder(res.Body).Decode(&response); err != nil {
					t.Fatal(err)
				}
				res.Body.Close()

				if !response.Success {
					t.Fatal("No error is expected when the same payment is inserted twice but 'Persist' call has not been executed yet")
				}
			})

			t.Run("Number of payments in the list must still be zero before calling Persist operation", func(t *testing.T) {
				ps, err := server.List()
				if err != nil {
					t.Fatal(err)
				}

				if len(ps.Data) != 0 {
					t.Fatalf("No payments should be found. Found %d payments", len(ps.Data))
				}
			})

			//We call persist by ourselves to have the control of where the data is persisted
			if err = server.Persist(); err != nil {
				t.Fatal(err)
			}

			t.Run("Number of payments in the list must be one now", func(t *testing.T) {
				ps, err := server.List()
				if err != nil {
					t.Fatal(err)
				}

				if len(ps.Data) != 1 {
					t.Fatalf("At least one payment should be found. Found %d payments", len(ps.Data))
				}

				t.Run("And the payment must have an amount of 100", func(t *testing.T){
					if ps.Data[0].Attributes.Amount != "100" {
						t.Fatalf("Expected 100 in amount, found %s", ps.Data[0].Attributes.Amount)
					}
				})
			})

			t.Run("Check duplicate payment creation after calling persist", func(t *testing.T) {
				reader := bytes.NewReader(byt)
				req, err := http.NewRequest(http.MethodPut, endpoint, reader)
				if err != nil {
					t.Fatalf("Error trying to create request: %s", err.Error())
				}

				if res, err = client.Do(req); err != nil {
					t.Fatal(err)
				}

				response := form3_cc.HTTPResponse{}
				if err = json.NewDecoder(res.Body).Decode(&response); err != nil {
					t.Fatal(err)
				}
				res.Body.Close()

				if response.Success {
					t.Fatal("An error is expected if you try to create the same Payment ID twice if the first Payment is already persisted")
				}
			})

			t.Run("Number of payments in the list must still be one", func(t *testing.T) {
				ps, err := server.List()
				if err != nil {
					t.Fatal(err)
				}

				if len(ps.Data) != 1 {
					t.Fatalf("At least one payment should be found. Found %d payments", len(ps.Data))
				}
			})
		})
	})

	t.Run("UPDATE Payment", func(t *testing.T){
		//Let's change the amount from 100 to 200
		payment1.Attributes.Amount = "200"

		if err = server.Update(&payment1); err != nil {
			t.Fatalf("Error trying to update payment %s", err.Error())
		}

		t.Run("FETCH a single payment", func(t *testing.T){
			paymentClone, err := server.Get(payment1.ID)
			if err != nil {
				t.Fatal(err)
			}

			if paymentClone.Attributes.Amount != "200" {
				t.Fatalf("Expected an amount of 200 after updating the payment. Found %s", paymentClone.Attributes.Amount)
			}
		})
	})

	t.Run("DELETE Payment", func(t *testing.T){
		if err = server.Delete(payment1.ID); err != nil {
			t.Fatal(err)
		}

		t.Run("Number of payments in the list must be zero again", func(t *testing.T) {
			ps, err := server.List()
			if err != nil {
				t.Fatal(err)
			}

			if len(ps.Data) != 0 {
				t.Fatalf("No payments should be found. Found %d payments", len(ps.Data))
			}
		})
	})

}
