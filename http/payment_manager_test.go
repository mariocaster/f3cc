package http

import (
	"testing"
	"github.com/sayden/form3-cc"
	"github.com/gin-gonic/gin"
	"net/http"
	"net"
	"bufio"
				"github.com/thehivecorporation/log"
	"fmt"
	"bytes"
	"io/ioutil"
	"encoding/json"
)

type mockPaymentManager struct {
	listCalls int
	createCalls int
}

func (m *mockPaymentManager) Create(p *form3_cc.Payment) error {
	m.createCalls++

	log.WithField("payment", fmt.Sprintf("%#v", p)).Info("Called 'Create' payment")
	return nil
}

func (m *mockPaymentManager) List() (form3_cc.PaymentResponse, error) {
	m.listCalls++
	return form3_cc.PaymentResponse{}, nil
}

func (m *mockPaymentManager) Get(id string) (*form3_cc.Payment, error) {
	panic("implement me")
}

func (m *mockPaymentManager) Delete(id string) error {
	panic("implement me")
}

func (m *mockPaymentManager) Update(p *form3_cc.Payment) error {
	panic("implement me")
}

func (m *mockPaymentManager) Persist() error {
	panic("implement me")
}

type responseWriterMock struct {
	t *testing.T
}

func (r *responseWriterMock) Header() http.Header {
	r.t.Log("Getting headers")

	return http.Header{}
}

func (r *responseWriterMock) Write([]byte) (int, error) {
	r.t.Log("Writing")
	return 0, nil
}

func (r *responseWriterMock) WriteHeader(statusCode int) {
	r.t.Log("Writing header")
}

func (r *responseWriterMock) Hijack() (net.Conn, *bufio.ReadWriter, error) {
	r.t.Log("Hijacking")

	return nil, nil, nil
}

func (r *responseWriterMock) Flush() {
	r.t.Log("Flushing")
}

func (r *responseWriterMock) CloseNotify() <-chan bool {
	r.t.Log("Closing")

	return nil
}

func (r *responseWriterMock) Status() int {
	r.t.Log("Getting status")
	return 200
}

func (r *responseWriterMock) Size() int {
	r.t.Log("Getting size")

	return 0
}

func (r *responseWriterMock) WriteString(string) (int, error) {
	r.t.Log("Writing string")

	return 0, nil
}

func (r *responseWriterMock) Written() bool {
	r.t.Log("Check written")
	return true
}

func (r *responseWriterMock) WriteHeaderNow() {
	r.t.Log("Writing header now")
}

func (r *responseWriterMock) Pusher() http.Pusher {
	r.t.Log("Getting pusher")
	return nil
}

func TestRESTPaymentGinServer(t *testing.T) {
	mockPM := &mockPaymentManager{}
	server := service{PaymentManager: mockPM}

	t.Run("Check HTTP List handler", func(t *testing.T){
		ginContext := gin.Context{
			Writer:&responseWriterMock{t},
			Request:&http.Request{},
		}

		server.ListHandler(&ginContext)

		if mockPM.listCalls != 1 {
			t.Fatal("A list call must be present")
		}
	})

	t.Run("Check HTTP Create handler", func(t *testing.T){
		payment := form3_cc.Payment{
			ID:"id1",
			Attributes:form3_cc.PaymentAttributes{
				Amount:"100",
			},
		}

		byt, err := json.Marshal(payment)
		if err != nil {
			t.Fatal(err)
		}

		reader := bytes.NewBuffer(byt)
		readerCloser := ioutil.NopCloser(reader)

		ginContext := gin.Context{
			Writer:&responseWriterMock{t},
			Request:&http.Request{
				Body:readerCloser,
			},
		}

		server.CreateHandler(&ginContext)

		if mockPM.createCalls != 1 {
			t.Fatal("At least a call to create must be found")
		}

		t.Run("Using an malformed JSON", func(t *testing.T){
			malformedJson :=byt[:100]

			reader := bytes.NewBuffer(malformedJson)
			readerCloser := ioutil.NopCloser(reader)

			ginContext := gin.Context{
				Writer:&responseWriterMock{t},
				Request:&http.Request{
					Body:readerCloser,
				},
			}

			server.CreateHandler(&ginContext)

			if mockPM.createCalls != 1 {
				t.Fatal("The number of calls to create must stay in one because the JSON is malformed")
			}
		})
	})

	//TODO an so on and so for...
}
