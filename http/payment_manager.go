package http

import (
	"github.com/gin-gonic/gin"
	"github.com/juju/errors"
	"github.com/sayden/form3-cc"
	"github.com/thehivecorporation/log"
	"net/http"
	"fmt"
	"os"
	"os/signal"
	"context"
	"time"
)

type service struct {
	form3_cc.PaymentManager
	router *gin.Engine
	server *http.Server
	port   int
}

func NewPaymentServer(port int, next form3_cc.PaymentManager) (*service, error) {
	if next == nil {
		return nil, errors.New(form3_cc.NO_PAYMENT_MANAGER_ERROR)
	}

	router := gin.Default()
	server := &service{next, router, nil, port}
	server.configureEndpoints()

	return server, nil
}

func (r *service) CreateHandler(c *gin.Context) {
	tx := new(form3_cc.Payment)
	var res form3_cc.HTTPResponse

	err := c.BindJSON(&tx)
	if err != nil {
		err = errors.Annotate(err, "Could not get tx JSON object")
		log.WithError(err).Error("Could not do payment")

		res.Error = err.Error()


		c.JSON(500, res)

		return
	}
	if err = r.Create(tx); err != nil {
		err = errors.Annotate(err, "Error creating payment")
		log.WithError(err).Error("Could not create payment operation")

		res.Error = err.Error()
		c.JSON(500, res)

		return
	}

	res.Success = true
	c.JSON(200, res)
}

func (r *service) UpdateHandler(c *gin.Context) {
	tx := new(form3_cc.Payment)
	var res form3_cc.HTTPResponse

	err := c.BindJSON(&tx)
	if err != nil {
		err = errors.Annotate(err, "Could not get tx JSON object")
		log.WithError(err).Error("Could not do payment")

		res.Error = err.Error()


		c.JSON(500, res)

		return
	}
	if err = r.Update(tx); err != nil {
		err = errors.Annotate(err, "Error updating payment")
		log.WithError(err).Error("Could not create payment operation")

		res.Error = err.Error()
		c.JSON(500, res)

		return
	}

	res.Success = true
	c.JSON(200, res)
}

func (r *service) ListHandler(c *gin.Context) {
	var res form3_cc.HTTPResponse

	txs, err := r.List()
	if err != nil {
		err = errors.Annotate(err, "Error creating payment")
		log.WithError(err).Error("Could not do payment")

		res.Error = err.Error()

		c.JSON(500, res)

		return
	}

	res.Success = true
	res.Result = txs

	c.JSON(200, res)
}

func (r *service) GetHandler(c *gin.Context) {
	var res form3_cc.HTTPResponse

	id := c.Param("id")

	payment, err := r.Get(id)
	if err != nil {
		err = errors.Annotate(err, "Could not retrieve payment")
		log.WithError(err).Error("Error getting payment")
		res.Error = err.Error()

		c.JSON(500, res)

		return
	}

	res.Success = true
	res.Result = form3_cc.PaymentResponse{
		Data: []form3_cc.Payment{*payment},
	}

	c.JSON(200, res)
}

func (r *service) DeleteHandler(c *gin.Context) {
	var res form3_cc.HTTPResponse

	id := c.Param("id")
	if err := r.Delete(id); err != nil {
		err = errors.Annotate(err, "Could not delete payment")
		log.WithError(err).Error("Error deleting payment")

		res.Error = err.Error()
		c.JSON(500, res)

		return
	}

	res.Success = true

	c.JSON(200, res)
}

func (r *service) configureEndpoints() {
	v1 := r.router.Group("/api/v1/payment")

	v1.PUT("/:id", r.UpdateHandler)
	v1.PUT("/", r.CreateHandler)
	v1.GET("/", r.ListHandler)
	v1.GET("/:id", r.GetHandler)
	v1.DELETE("/:id", r.DeleteHandler)
}

func (r *service) Run() {
	r.server = &http.Server{
		Addr:fmt.Sprintf(":%d", r.port),
		Handler:r.router,
	}

	go func(){
		if err := r.server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.WithError(err).Error("Unexpected server error")
		}
	}()

	quit :=make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit

	r.Stop()
}

func (r *service) Stop(){
	log.Info("Shutting down server")

	ctx, cancel := context.WithTimeout(context.Background(), 2* time.Second)
	defer cancel()
	if err := r.server.Shutdown(ctx); err != nil {
		log.WithError(err).Fatal("Error trying to shutdown server")
	}

	log.Info("Server closed")
}