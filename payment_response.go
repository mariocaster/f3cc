package form3_cc

type PaymentResponse struct {
	Data  []Payment `json:"data"`
	Links struct {
		Self string `json:"self"`
	} `json:"links"`
}
