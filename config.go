package form3_cc

// Error messages
const (
	NO_PAYMENT_MANAGER_ERROR = "No PaymentManager injected. At least one Payment strategy must be passed in to create the server"
)

type Config struct {
	Port int
}
