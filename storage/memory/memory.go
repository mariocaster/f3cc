package memory

import (
	"github.com/juju/errors"
	"github.com/sayden/form3-cc"
	"sync"
)

type storage struct {
	payments map[string]*form3_cc.Payment
	queue    []*form3_cc.Payment
	sync.RWMutex
}

func New() form3_cc.PaymentManager {
	return &storage{
		payments: make(map[string]*form3_cc.Payment),
		queue:    make([]*form3_cc.Payment, 0),
	}
}

func (s *storage) Create(p *form3_cc.Payment) error {
	s.Lock()
	defer s.Unlock()

	if _, ok := s.payments[p.ID]; ok {
		return errors.Errorf("A Payment with ID '%s' already exists", p.ID)
	}
	s.queue = append(s.queue, p)

	return nil
}

func (s *storage) List() (pr form3_cc.PaymentResponse, err error) {
	ps := make([]form3_cc.Payment, len(s.payments))

	var i int

	s.RLock()
	defer s.RUnlock()

	for _, v := range s.payments {
		ps[i] = *v
		i++
	}

	pr.Data = ps

	return
}

func (s *storage) Get(id string) (*form3_cc.Payment, error) {
	s.RLock()
	defer s.RUnlock()

	if payment, ok := s.payments[id]; ok {
		return payment, nil
	}

	return nil, errors.Errorf("Payment with ID '%s' not found", id)
}

func (s *storage) Delete(id string) error {
	s.Lock()
	defer s.Unlock()

	if _, ok := s.payments[id]; !ok {
		return errors.Errorf("Payment with ID '%s' not found", id)
	}

	delete(s.payments, id)

	return nil
}

func (s *storage) Update(p *form3_cc.Payment) error {
	s.Lock()
	defer s.Unlock()

	if _, ok := s.payments[p.ID]; !ok {
		return errors.Errorf("Payment with ID '%s' not found", p.ID)
	}

	s.payments[p.ID] = p

	return nil
}

func (s *storage) Persist() error {
	s.Lock()
	defer s.Unlock()

	//TODO Here you will remove duplicates as well as do corrections in the transactions. It is assumed that the queue
	// is updated and emptied after this operation

	for _, p := range s.queue {
		s.payments[p.ID] = p
	}

	s.queue = s.queue[:0]

	return nil
}
