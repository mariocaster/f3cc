package form3_cc

// PaymentManager is the interface that must be implemented to deal with Payments within the system
type PaymentManager interface {
	//Create operation enqueues Payments in memory and a Persist call must be used to flush them
	Create(p *Payment) error
	List() (PaymentResponse, error)
	Get(id string) (*Payment, error)
	Delete(id string) error
	Update(p *Payment) error

	//Persist is called
	Persist() error
}

