# Form3 Code Challenge

First of all, thank your for this good opportunity to be part of a company like Form3. I really think that it's a challenging project that can make a professional like me grow by being part of it.

## Brief description

A simple solution written in Go for a REST API using Gin Gonic by embracing the decorator pattern to wrap each REST call. An interface called `PaymentManager` with the main 6 actions (CRUD + list and persist) must be implemented by every entity that wants to interact with the Payments system.

## Code structure

```shell
.
├── cli
│   ├── black_box_test.go
│   ├── cli.go
│   ├── main.go
│   └── orchestrate.go
├── config.go
├── http
│   ├── payment_maanger_test.go
│   └── payment_manager.go
├── http_response.go
├── logging
│   └── logging.go
├── payment.go
├── payment_manager.go
├── payment_response.go
└── storage
    └── memory
        └── memory.go
```

Business entities are in the root, this is to help reuse of this package in other go packages by simply importing `github.com/sayden/form3_cc` (in the example). So `payment.go` has the *Payment* business entity and `payment_response.go` the entity that is returned homogeneusly on each fetch operation on the server (fetch by id or list).

An special interface called `PaymentManager` is also present so that each operation on the server must satisfy this interface. This is to ensure consistency and encapsulation between the decorators.

`main` package is in `cli` folder. Has become almost a convention to structure Go programs this way so that you write most of the code as a library instead of a full program.

`cli` folder has also `cli.go` to configure the Command Line Interface of the server execution with shell params and environment variables. This is very convenient to use Docker and Kubernetes later. `orchestrate.go` makes all calls needed to initiate the proper configuration of the server and launches it.

Finally `logging` and `storage` are the packages that the *orchestrator* is going to use to launch the server. 

* As it names implies, `logging` contains `stdout` structured logging plus telemetry by using a logging library that enables both (`github.com/thehivecorporation/log` which is done and maintained by myself).
* `storage` has strategies for persistence. A very simple and naive **memory** persistence is implemented but any kind of persistence could be used. For the extremely delicate purpose of Transactions I would use a strongly consistent distributed database database that allows partitioning based on some input data of the user like the N numbers of his debit card ordered lexicographically in a BigTable / HBase, use a distributed MySQL with a similar configuration or simply use Google Spanner.

This structure needs way less tests or, at least, they will be quite redundant in many cases because each piece has such a level of isolation that it's almost useless to test each package separatedly (you'd be testing Go standard library instead of your own source code). So a full end-to-end is the most useful and reliable way to test the orchestrator. Anyways, on `cli/black_box_test.go` you can check an almost end to end test by launching the server on the background. On `http/payment_manager_test.go` you 
can check the behaviour of the specific HTTP server (which, in any case, is also tested by using the black box technique)

The system works in a very straightforward way (the beauty of simplicity?). The orchestrator creates the instances of the server, logging and storage (in this case, memory storage) and "joins" them to create a chain of processing like `server -> logging -> metrics -> storage -> metrics -> logging -> server -> logging`. It's very easy to add new features to the system by chaining more implementations of `PaymentManager`.

## Assumptions

In this code challenge I have assumed the following points:

* For the sake of simplicity, it's assumed that a Payment is a Transaction+Payment operation. Means, we are assuming that each transaction has a single payment and viceversa (this is not like this, of course, you could have an attempt of transaction that isn't validated and that doesn't generate any payment)
* Payment creation is separated from Payment persistence as described in the document. I guess this is because they are queued in any kind of single partition Kafka or RabbitMQ so tha it helps in deduplication and consistency. For the purpose of the example, an endpoint to Persist isn't created, instead, the data is queued in memory and persisted in a map every 5 seconds.
* Code is done to show extensibility, design and robustness so some parts are commented as TODO if they are redundant to ease the reader the task of reading the codebase.
* As part of the robustness of the system, I've also implemented a logging and telemetry package. I consider that proper logging and metrics are part of a robust system so I wouldn't consider them optional nor extra feature. However, I left tracing as a TODO to simplify codebase reading a bit. I have been using Google Cloud Platform, Datadog and recently OpenCensus for this purpose, something I guess that you're doing by using Linkerd and Zipkin.
* Sidecar configuration to form a mesh with Linkerd has been omitted. I know the product but I have more experience with plain old Nginx, Envoy and, recently, with Istio
* No tricky floating points maths have been implemented. Mainly because it's a topic by itself and there are a dozen of ways to deal with floating point errors. For example, I have used one that instead of using strings, we had a in-memory map to convert currencies with floating point to whole numbers to make operations.

## Thanks
