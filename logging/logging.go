package logging

import (
	"github.com/juju/errors"
	"github.com/sayden/form3-cc"
	"github.com/thehivecorporation/log"
	"time"
)

type Logger struct {
	next form3_cc.PaymentManager
}

func New(next form3_cc.PaymentManager) (form3_cc.PaymentManager, error) {
	if next == nil {
		return nil, errors.New(form3_cc.NO_PAYMENT_MANAGER_ERROR)
	}

	return &Logger{next}, nil
}

func (l *Logger) Create(t *form3_cc.Payment) error {
	start := time.Now()
	defer func(start time.Time) {
		log.WithTag("country", "england").Histogram("create", time.Since(start).Seconds()).
			WithField("elapsed", time.Since(start).Seconds()).Debug("Creating payment")
	}(start)

	return l.next.Create(t)
}

func (l *Logger) Update(p *form3_cc.Payment) error {
	start := time.Now()
	defer func(start time.Time) {
		log.WithTag("country", "england").Histogram("update", time.Since(start).Seconds()).
		WithField("elapsed", time.Since(start).Seconds()).Debugf("Updating payment '%s'", p.ID)
	}(start)

	return l.next.Update(p)
}

func (l *Logger) List() (form3_cc.PaymentResponse, error) {
	start := time.Now()
	defer func(start time.Time) {
		log.WithTag("country", "england").Histogram("list", time.Since(start).Seconds()).
		WithField("elapsed", time.Since(start).Seconds()).Debug("Listing payments")
	}(start)

	return l.next.List()
}

func (l *Logger) Get(id string) (*form3_cc.Payment, error) {
	start := time.Now()
	defer func(start time.Time) {
		log.WithTag("country", "england").Histogram("get", time.Since(start).Seconds()).
		WithField("elapsed", time.Since(start).Seconds()).Debug("Getting payment")
	}(start)

	return l.next.Get(id)
}

func (l *Logger) Delete(id string) error {
	start := time.Now()
	defer func(start time.Time) {
		log.WithTag("country", "england").Histogram("delete", time.Since(start).Seconds()).
		WithField("elapsed", time.Since(start).Seconds()).Debugf("Deleting payment '%s'", id)
	}(start)

	return l.next.Delete(id)
}

func (l *Logger) Persist() error {
	start := time.Now()
	defer func(start time.Time) {
		log.WithTag("country", "england").Histogram("persist", time.Since(start).Seconds()).
		WithField("elapsed", time.Since(start).Seconds()).Debugf("Persisting current payments")
	}(start)

	return l.next.Persist()
}
