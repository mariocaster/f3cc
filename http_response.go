package form3_cc

type HTTPResponse struct {
	Success bool                     `json:"success"`
	Error   string                   `json:"error"`
	Result  PaymentResponse `json:"result"`
}
